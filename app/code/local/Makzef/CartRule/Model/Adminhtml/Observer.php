<?php

/**
 * Admin Observer
 *
 * @category  Makzef
 * @package   Makzef_CartRule
 * @author    Maksym Pletiuk <makspletiuk@gmail.com>
 * @copyright 2019
 */
class Makzef_CartRule_Model_Adminhtml_Observer
{
    protected $_productAttributes = null;

    /**
     * Adding new option to apply rule and adding field for product attribute
     *
     * @event adminhtml_block_html_before
     * @param Varien_Event_Observer $observer
     */
    public function addFixedPriceFromProductAttribute(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Block_Template $block */
        $block = $observer->getBlock();
        if ($block instanceof Mage_Adminhtml_Block_Promo_Quote_Edit_Tab_Actions) {
            /** @var Varien_Data_Form $form */
            $form = $block->getForm();
            /** @var Mage_SalesRule_Model_Rule $model */
            $model = Mage::registry('current_promo_quote_rule');

            /** @var Varien_Data_Form_Element_Fieldset $fieldset */
            $fieldset = $form->getElements()->searchById('action_fieldset');
            $fieldset->removeField('simple_action');

            $fieldset->addField('simple_action', 'select',
                [
                    'label' => Mage::helper('salesrule')->__('Apply'),
                    'name' => 'simple_action',
                    'onchange' => 'if (this.selectedIndex && this.options[this.selectedIndex].value == \'fixed_by_product_attribute\') {
                        $(\'rule_product_attribute_action\').up().up().show()
                    } else {
                        $(\'rule_product_attribute_action\').up().up().hide()
                    }',
                    'options' => [
                        Mage_SalesRule_Model_Rule::BY_PERCENT_ACTION => Mage::helper('salesrule')
                            ->__('Percent of product price discount'),
                        Mage_SalesRule_Model_Rule::BY_FIXED_ACTION => Mage::helper('salesrule')
                            ->__('Fixed amount discount'),
                        Mage_SalesRule_Model_Rule::CART_FIXED_ACTION => Mage::helper('salesrule')
                            ->__('Fixed amount discount for whole cart'),
                        Mage_SalesRule_Model_Rule::BUY_X_GET_Y_ACTION => Mage::helper('salesrule')
                            ->__('Buy X get Y free (discount amount is Y)'),
                        Makzef_CartRule_Helper_Data::FIXED_BY_ATTRIBUTE_ACTION => Mage::helper('cartrule')
                            ->__('Fixed amount from product attribute'),
                    ],
                ], '^');

            $fieldset->addField('product_attribute_action', 'select',
                [
                    'label' => Mage::helper('cartrule')->__('Product Attribute'),
                    'name' => 'product_attribute_action',
                    'options' => $this->_getProductAttributes(),
                    'after_element_html' => '<script>(function () {
                        $(\'rule_product_attribute_action\').up().up().hide();
                        if ($(\'rule_simple_action\').value == \'fixed_by_product_attribute\') {
                            $(\'rule_product_attribute_action\').up().up().show();
                        }
                    })()</script>'
                ], 'simple_action');

            $form->setValues($model->getData());
            $block->setForm($form);
        }


    }


    /**
     * @return array|null
     */
    protected function _getProductAttributes()
    {
        if (is_null($this->_productAttributes)) {
            $this->_productAttributes = [];
            $productAttributes = Mage::getResourceModel('catalog/product_attribute_collection');
            foreach ($productAttributes as $attribute) {
                /** @var Mage_Eav_Model_Attribute $attribute */
                if ($attribute->getStoreLabel()) {
                    $this->_productAttributes[$attribute->getAttributeCode()] = $attribute->getStoreLabel();
                }
            }
        }

        return $this->_productAttributes;
    }
}