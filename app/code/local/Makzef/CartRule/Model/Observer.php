<?php

/**
 * Observer
 *
 * @category  Makzef
 * @package   Makzef_CartRule
 * @author    Maksym Pletiuk <makspletiuk@gmail.com>
 * @copyright 2019
 */
class Makzef_CartRule_Model_Observer
{
    /**
     * If rule apply by 'fixed amount by product attribute' replace discount amount
     * value by product attribute value.
     *
     * @event salesrule_validator_process
     * @param Varien_Event_Observer $observer
     */
    public function applyDiscountFromProductAttribute(Varien_Event_Observer $observer)
    {
        /** @var Mage_SalesRule_Model_Rule $rule */
        $rule = $observer->getRule();
        if ($rule->getSimpleAction() == Makzef_CartRule_Helper_Data::FIXED_BY_ATTRIBUTE_ACTION) {
            /** @var Mage_Catalog_Model_Product $product */
            $productId = $observer->getItem()->getProductId();
            $storeId = $observer->getItem()->getStoreId();
            $attributeValue = Mage::getResourceModel('catalog/product')
                ->getAttributeRawValue(
                    $productId,
                    Makzef_CartRule_Helper_Data::PRODUCT_ATTRIBUTE_CART_DISCOUNT,
                    $storeId
                );

            /** @var Varien_Object $result */
            $result = $observer->getResult();
            $result->setDiscountAmount($attributeValue);
            $result->setBaseDiscountAmount($attributeValue);
        }
    }

}