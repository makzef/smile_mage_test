<?php

/**
 * Helper
 *
 * @category  Makzef
 * @package   Makzef_CartRule
 * @author    Maksym Pletiuk <makspletiuk@gmail.com>
 * @copyright 2019
 */
class Makzef_CartRule_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * New rule type action
     */
    const FIXED_BY_ATTRIBUTE_ACTION = 'fixed_by_product_attribute';
    /**
     * New product attribute
     */
    const PRODUCT_ATTRIBUTE_CART_DISCOUNT = 'cart_price_rule_discount';

}