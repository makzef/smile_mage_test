<?php

/**
 * Migration
 *
 * @category  Makzef
 * @package   Makzef_CartRule
 * @author    Maksym Pletiuk <makspletiuk@gmail.com>
 * @copyright 2019
 */

/** @var Makzef_CartRule_Model_Setup $installer */
$installer = $this;

$installer->startSetup();

$installer->addAttribute(
    'catalog_product',
    Makzef_CartRule_Helper_Data::PRODUCT_ATTRIBUTE_CART_DISCOUNT,
    [
        'type' => 'decimal',
        'label' => 'Cart Price Rule Discount',
        'input' => 'price',
        'backend' => 'catalog/product_attribute_backend_price',
        'required' => false,
        'sort_order' => 50,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
        'group' => 'Prices',
        'system' => false,
        'user_defined' => true
    ]
);

$installer->endSetup();